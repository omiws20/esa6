#! env/bin/python3
# file: GetPlayList.py

import requests
from bs4 import BeautifulSoup

class Song:

    def __init__(self, interpret, title, playDate):
        self.interpret = interpret
        self.title = title
        self.playDate = playDate

    def __repr__(self) -> str:
        return f"Interpret: {self.interpret} Title: {self.title} *** Playdate: {self.playDate}"

def getSWR1Playlist(url):
    songs = []

    req = requests.get(url)

    bs = BeautifulSoup(req.text,features="html.parser")

    plItems = bs.find_all("div", {"class": "list-playlist-item"})

    for plItem in plItems:
        ddTitle = plItem.find_all("dd", {"class": "playlist-item-song"})[0].text.strip()
        ddInterpret = plItem.find_all("dd", {"class": "playlist-item-artist"})[0].text.strip()
        timPlayDate = plItem.find("time")["datetime"]
        songs.append(Song(ddInterpret, ddTitle, timPlayDate))

    return songs

if __name__ == "__main__":
    url = "https://www.swr.de/swr1/bw/playlist/index.html"
    songs = getSWR1Playlist(url)
    for song in songs:
        print(song)


