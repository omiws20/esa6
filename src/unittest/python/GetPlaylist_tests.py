import unittest

from GetPlaylist import getSWR1Playlist

class GetPlayListTest(unittest.TestCase):

    def test_should_return_list(self):
        url = "https://www.swr.de/swr1/bw/playlist/index.html"
        songs = getSWR1Playlist(url)
        self.assertTrue(len(songs) > 0)
